var $output;

function onDeviceReady(){
    $output = $('#output');

    logMessage("Device Ready");

    navigator.geolocation.getCurrentPosition(onPosition, onPositionError, {
        maxAge: 0,
        timeout: 10000,
        enableHighAccuracy: false
    });
}

function logMessage(msg){
    $output.append("<p>"+msg+"</p>");
}

function logError(msg){
    $output.append("<p class=\"error\">"+msg+"</p>");
}

function onPosition(position){
    logMessage("Position received: "+JSON.stringify(position));
}

function onPositionError(error){
    logError("Position error: "+JSON.stringify(error));
}

$(document).on("deviceready", onDeviceReady);